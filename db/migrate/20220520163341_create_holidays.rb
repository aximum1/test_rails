class CreateHolidays < ActiveRecord::Migration[7.0]
  def change
    create_table :holidays do |t|
      t.date :date_start
      t.date :date_end
      t.references :user, null: false, foreign_key: true   
    end
  end
end
