class AddAasmStateToHolidays < ActiveRecord::Migration[7.0]
  def change
    add_column :holidays, :aasm_state, :string
  end
end
