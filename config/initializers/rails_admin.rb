RailsAdmin.config do |config|
  config.asset_source = :sprockets

  # config.parent_controller = '::RailsAdminController'

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == CancanCan ==
  # config.authorize_with :cancancan

  
  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    state

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  
  end

  config.authorize_with do
      redirect_to main_app.root_path unless current_user.try(:admin?)
  end

  config.model 'User' do
    list do
      field :username do
        label "Имя, фамилия"
        formatted_value do 
          value.to_s.upcase
        end
      end  
      field :email do
        formatted_value do 
          value.to_s.upcase
        end
      end
      field :holidays do
        label "Выходные дни"
      end
   
    end
  end

  config.model Holiday do
    list do
      configure :username do
      end
      fields :id do
        label "Айди"
      end
      fields :username do
        label "Имя, фамилия"
        formatted_value do 
          value.to_s.upcase
        end
      end
      fields :date_start do
        label "Начало отпуска"
        formatted_value do 
          value.strftime("%d.%m.%Y")
        end
      end
      fields :date_end do
        label "Конец отпуска"
        formatted_value do 
          value.strftime("%d.%m.%Y")
        end
      end
      field :aasm_state, :state do 
        label "Статус"
      end
      # fields :user
    end
  end

end
