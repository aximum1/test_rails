Rails.application.routes.draw do
  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root "users#index"

  resources :users do
    resources :holidays
  end
end
