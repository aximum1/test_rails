class HolidaysController < ApplicationController
  before_action :authenticate_user!
  
  def create
    @user = User.find(params[:user_id])
    @holiday =  @user.holidays.new(holiday_params)
    
    if @holiday.save
      redirect_to user_path(@user)
      flash[:notice] = "Запись отправлена для согласования"
    else
      render user_path(:show)  
    end
  end
  
  private
    def holiday_params
      params.require(:holiday).permit(:date_start, :date_end)
    end
end
