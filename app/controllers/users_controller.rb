class UsersController < ApplicationController
  before_action :authenticate_user!
  
  
  def index
    @users = User.all
    # @users = User.joins(:holidays).all

    if current_user.admin?
     redirect_to "/admin"
    else
      redirect_to "/users/#{current_user.id}"
    end
  end
 
  def show
    @user = User.find(params[:id])
    @holiday = @user.holidays.new
    if current_user.admin?
      redirect_to "/admin"
    end
  end

  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to users_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to root_path
  end

private
  def user_params
    params.require(:user).permit(:username, :password)
  end
end

