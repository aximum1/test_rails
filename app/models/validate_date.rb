class ValidateDate < ActiveModel::Validator

  def validate(holiday)
    
    if  Holiday.date_validation_being_reviewed(holiday.date_start, holiday.date_end) != []  
      holiday.errors.add :base, :invalid, message: "Ошибка: указанные даты пересекаются с датами, принятыми к рассмотрению."
    elsif Holiday.date_validation(holiday.date_end, holiday.date_start) != [] 
      holiday.errors.add :base, :invalid, message: "Ошибка: указанные даты пересекаются с уже согласованными датами."
    end
    # byebug
  end
end