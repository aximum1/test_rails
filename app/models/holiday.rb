class Holiday < ApplicationRecord
  
  ## Обработка state_machine
  include AASM

  aasm do
    state :being_reviewed, display: 'На рассмотрении'
    state :accepted, display: 'Принято'
    state :rejected, display: 'Отказано'
   
    event :accept do
      transitions from: :being_reviewed, to: :accepted
    end

    event :reject do
      transitions from: :being_reviewed, to: :rejected
    end
  end

  belongs_to :user 

  ## Сортировка дат отпуска
  default_scope { order(date_start: :desc, aasm_state: :asc)}

  ##

  def username
    self.user.username
  end

  ## Валидация дат отпуска

  validates_presence_of :date_start, :date_end
  
  validates :date_end, comparison: { greater_than_or_equal_to: :date_start }

  scope :date_validation, -> (date_end, date_start) { where("(date_start <= ? and ? <= date_end and aasm_state = 'being_reviewed') or (date_start <= ? and ? <= date_end and aasm_state = 'accepted')", date_end, date_start, date_end, date_start )}

  # scope :date_validation_two, -> (date_end, date_start) { where("date_start <= ? and ? <= date_end and aasm_state = 'accepted'", date_end, date_start )}

  scope :date_validation_being_reviewed, -> (date_start, date_end) { where("date_start = ? and date_end = ? and aasm_state = 'being_reviewed'", date_start, date_end)}

  include ActiveModel::Validations
  validates_with ValidateDate, on: :create
  # byebug
end